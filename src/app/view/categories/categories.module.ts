import { NgModule } from '@angular/core';
import { CategoriesRoutingModule } from './categories-routing.module';
import { ListCategoriesComponent } from './list-categories/list-categories.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormCategoryComponent } from './form-category/form-category.component';

@NgModule({
  imports: [
    CategoriesRoutingModule,
    SharedModule,
  ],
  declarations: [
    ListCategoriesComponent,
    FormCategoryComponent,
  ]
})
export class CategoriesModule { }
