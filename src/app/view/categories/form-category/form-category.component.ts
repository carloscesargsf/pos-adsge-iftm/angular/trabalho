import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/core/services/categories/categories.service';
import { Category } from 'src/app/core/models/category.model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-category',
  templateUrl: './form-category.component.html',
  styleUrls: ['./form-category.component.scss']
})
export class FormCategoryComponent implements OnInit {

  id: number;
  category: FormGroup;

  constructor(private categoriesService: CategoriesService,
              private currentRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    // tslint:disable-next-line: no-string-literal
    this.currentRoute.params.subscribe(
      params => {
        this.category = Category.getFormGroup();
        // tslint:disable-next-line: no-string-literal
        this.id = params['id'];
        if (this.id) {
          this.categoriesService.findById(this.id).subscribe(
            category => this.category.patchValue(category),
            () => this.router.navigate(['/404']),
          );
        }
      },
    );
  }

  onSubmit() {
    if (this.category.valid) {
      if (!this.id) {
        this.categoriesService.create(this.category.value).subscribe(
          () => this.router.navigate(['/categories']),
          error => console.error('create error', error),
        );
      } else {
        this.categoriesService.update(this.id, this.category.value).subscribe(
          () => this.router.navigate(['/categories']),
          error => console.error('update error', error),
        );
      }
    } else {
      console.error(this.category.errors);
    }
  }

  onReset() {
    this.category.reset();
  }

}
