import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/core/services/categories/categories.service';
import { Category } from 'src/app/core/models/category.model';
import { PageInfo } from 'src/app/core/models/page-info';

@Component({
  selector: 'app-list-categories',
  templateUrl: './list-categories.component.html',
  styleUrls: ['./list-categories.component.scss']
})
export class ListCategoriesComponent implements OnInit {

  payLoad: PageInfo<Category> = new PageInfo<Category>();

  constructor(private categoriesService: CategoriesService) {
  }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.categoriesService.findAll()
      .subscribe(payLoad => this.payLoad = payLoad);
  }

  delete(id: number) {
    this.categoriesService.delete(id).subscribe(
      () => this.fetchData(),
      error => console.warn(error),
    );
  }

}
