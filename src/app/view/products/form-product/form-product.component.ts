import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/core/services/products/products.service';
import { Product } from 'src/app/core/models/product.model';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-product',
  templateUrl: './form-product.component.html',
  styleUrls: ['./form-product.component.scss']
})
export class FormProductComponent implements OnInit {

  id: number;
  product: FormGroup;

  constructor(private productsService: ProductsService,
              private currentRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    // tslint:disable-next-line: no-string-literal
    this.currentRoute.params.subscribe(
      params => {
        this.product = Product.getFormGroup();
        // tslint:disable-next-line: no-string-literal
        this.id = params['id'];
        if (this.id) {
          this.productsService.findById(this.id).subscribe(
            product => this.product.patchValue(product),
            () => this.router.navigate(['/404']),
          );
        }
      },
    );
  }

  onSubmit() {
    if (this.product.valid) {
      if (!this.id) {
        this.productsService.create(this.product.value).subscribe(
          () => this.router.navigate(['/products']),
          error => console.error('create error', error),
        );
      } else {
        this.productsService.update(this.id, this.product.value).subscribe(
          () => this.router.navigate(['/products']),
          error => console.error('update error', error),
        );
      }
    } else {
      console.error(this.product.errors);
    }
  }

  onReset() {
    this.product.reset();
  }

}
