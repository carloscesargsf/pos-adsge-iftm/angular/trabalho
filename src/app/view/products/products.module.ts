import { NgModule } from '@angular/core';
import { ProductsRoutingModule } from './products-routing.module';
import { ListProductsComponent } from './list-products/list-products.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormProductComponent } from './form-product/form-product.component';

@NgModule({
  imports: [
    ProductsRoutingModule,
    SharedModule,
  ],
  declarations: [
    ListProductsComponent,
    FormProductComponent,
  ]
})
export class ProductsModule { }
