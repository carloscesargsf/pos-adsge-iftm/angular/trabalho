import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/core/services/products/products.service';
import { Product } from 'src/app/core/models/product.model';
import { PageInfo } from 'src/app/core/models/page-info';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.scss']
})
export class ListProductsComponent implements OnInit {

  payLoad: PageInfo<Product> = new PageInfo<Product>();

  constructor(private productsService: ProductsService) {
  }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.productsService.findAll()
      .subscribe(payLoad => this.payLoad = payLoad);
  }

  delete(id: number) {
    this.productsService.delete(id).subscribe(
      () => this.fetchData(),
      error => console.warn(error),
    );
  }

}
