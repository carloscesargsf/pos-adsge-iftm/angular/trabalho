import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListProductsComponent } from './list-products/list-products.component';
import { FormProductComponent } from './form-product/form-product.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Products'
    },
    children: [
      {
        path: '',
        component: ListProductsComponent,
        data: {
          title: 'List Products',
        },
      },
      {
        path: 'new',
        component: FormProductComponent,
        data: {
          title: 'New Product',
        },
      },
      {
        path: ':id',
        component: FormProductComponent,
        data: {
          title: 'Edit Product',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
