import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { Credentials } from 'src/app/core/models/credentials.model';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  credentials: FormGroup;
  returnUrl: string;
  error: string;

  constructor(private authService: AuthService,
              private currentRoute: ActivatedRoute,
              private router: Router) {
    if (this.authService.getTokenValue()) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.credentials = Credentials.getFormGroup();
    this.returnUrl = this.currentRoute.snapshot.queryParams.returnUrl || '/';
    this.error = '';
  }

  onSubmit() {
    if (this.credentials.valid) {
      this.authService.login(this.credentials.value)
        .pipe(first())
        .subscribe(
          () => this.router.navigate([this.returnUrl]),
          error => {
            this.error = error;
          }
        );
    } else {
      console.error(this.credentials.errors);
    }
  }

}
