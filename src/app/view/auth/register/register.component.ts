import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { User } from 'src/app/core/models/user.model';
import { first } from 'rxjs/operators';
import { UsersService } from 'src/app/core/services/users/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  user: FormGroup;
  error: string;

  constructor(private userService: UsersService,
              private router: Router) { }

  ngOnInit() {
    this.user = User.getFormGroup();
  }

  onSubmit() {
    if (this.user.valid) {
      this.userService.create(this.user.value)
        .pipe(first())
        .subscribe(
          () => this.router.navigate(['/login']),
          error => {
            this.error = error;
          }
        );
    } else {
      console.error(this.user.errors);
    }
  }

}
