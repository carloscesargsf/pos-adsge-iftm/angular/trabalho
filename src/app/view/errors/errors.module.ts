import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { Status404Component } from './status-404/status-404.component';

@NgModule({
  declarations: [
    Status404Component
  ],
  imports: [
    SharedModule
  ]
})
export class ErrorsModule { }
