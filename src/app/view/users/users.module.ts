import { NgModule } from '@angular/core';
import { UsersRoutingModule } from './users-routing.module';
import { ListUsersComponent } from './list-users/list-users.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormUserComponent } from './form-user/form-user.component';

@NgModule({
  imports: [
    UsersRoutingModule,
    SharedModule,
  ],
  declarations: [
    ListUsersComponent,
    FormUserComponent,
  ]
})
export class UsersModule { }
