import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListUsersComponent } from './list-users/list-users.component';
import { FormUserComponent } from './form-user/form-user.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Users'
    },
    children: [
      {
        path: '',
        component: ListUsersComponent,
        data: {
          title: 'List Users',
        },
      },
      {
        path: 'new',
        component: FormUserComponent,
        data: {
          title: 'New User',
        },
      },
      {
        path: ':id',
        component: FormUserComponent,
        data: {
          title: 'Edit User',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
