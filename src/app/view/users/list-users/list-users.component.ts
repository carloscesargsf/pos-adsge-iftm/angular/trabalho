import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/core/services/users/users.service';
import { User } from 'src/app/core/models/user.model';
import { PageInfo } from 'src/app/core/models/page-info';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {

  payLoad: PageInfo<User> = new PageInfo<User>();

  constructor(private usersService: UsersService) {
  }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.usersService.findAll()
      .subscribe(payLoad => this.payLoad = payLoad);
  }

  delete(id: number) {
    this.usersService.delete(id).subscribe(
      () => this.fetchData(),
      error => console.warn(error),
    );
  }

}
