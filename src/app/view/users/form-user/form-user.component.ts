import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/core/services/users/users.service';
import { User } from 'src/app/core/models/user.model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-user',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.scss']
})
export class FormUserComponent implements OnInit {

  id: number;
  user: FormGroup;

  constructor(private usersService: UsersService,
              private currentRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    // tslint:disable-next-line: no-string-literal
    this.currentRoute.params.subscribe(
      params => {
        this.user = User.getFormGroup();
        // tslint:disable-next-line: no-string-literal
        this.id = params['id'];
        if (this.id) {
          this.usersService.findById(this.id).subscribe(
            user => this.user.patchValue(user),
            () => this.router.navigate(['/404']),
          );
        }
      },
    );
  }

  onSubmit() {
    if (this.user.valid) {
      if (!this.id) {
        this.usersService.create(this.user.value).subscribe(
          () => this.router.navigate(['/users']),
          error => console.error('create error', error),
        );
      } else {
        this.usersService.update(this.id, this.user.value).subscribe(
          () => this.router.navigate(['/users']),
          error => console.error('update error', error),
        );
      }
    } else {
      console.error(this.user.errors);
    }
  }

  onReset() {
    this.user.reset();
  }

}
