import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsModule } from './products/products.module';
import { ErrorsModule } from './errors/errors.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [
    ProductsModule,
    ErrorsModule
  ]
})
export class ViewModule { }
