import { Component, OnInit } from '@angular/core';
import { OrdersService } from 'src/app/core/services/orders/orders.service';
import { Order } from 'src/app/core/models/order.model';
import { OrderStatus } from 'src/app/core/enums/order-status.enum';

@Component({
  selector: 'app-list-orders',
  templateUrl: './list-orders.component.html',
  styleUrls: ['./list-orders.component.scss']
})
export class ListOrdersComponent implements OnInit {

  payLoad: Order[] = [];

  constructor(private ordersService: OrdersService) {
  }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.ordersService.myOrders().subscribe(
      (payLoad: Order[]) => this.payLoad = payLoad,
    );
  }

  getOrderStatusLabel(orderStatus: string): string {
    return OrderStatus[orderStatus].label;
  }

}
