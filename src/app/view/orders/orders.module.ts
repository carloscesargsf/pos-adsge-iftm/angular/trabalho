import { NgModule } from '@angular/core';
import { OrdersRoutingModule } from './orders-routing.module';
import { ListOrdersComponent } from './list-orders/list-orders.component';
import { ListOrderItemsComponent } from './list-order-items/list-order-items.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    OrdersRoutingModule,
    SharedModule,
  ],
  declarations: [
    ListOrdersComponent,
    ListOrderItemsComponent,
  ]
})
export class OrdersModule { }
