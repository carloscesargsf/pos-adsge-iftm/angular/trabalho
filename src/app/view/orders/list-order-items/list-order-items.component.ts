import { Component, OnInit } from '@angular/core';
import { OrdersService } from 'src/app/core/services/orders/orders.service';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderItem } from 'src/app/core/models/order-item.model';

@Component({
  selector: 'app-list-orders',
  templateUrl: './list-order-items.component.html',
  styleUrls: ['./list-order-items.component.scss']
})
export class ListOrderItemsComponent implements OnInit {

  orderId: number;
  orderItems: OrderItem[] = [];

  constructor(private ordersService: OrdersService,
              private currentRoute: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.currentRoute.params.subscribe(
      params => {
        // tslint:disable-next-line: no-string-literal
        this.orderId = params['orderId'];
        if (this.orderId) {
          this.ordersService.getOrderItems(this.orderId).subscribe(
            orderItems => this.orderItems = orderItems,
            () => this.notFound(),
          );
        } else {
          this.notFound();
        }
      },
    );
  }

  notFound() {
    this.router.navigate(['/404']);
  }

}
