import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListOrdersComponent } from './list-orders/list-orders.component';
import { ListOrderItemsComponent } from './list-order-items/list-order-items.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Orders'
    },
    children: [
      {
        path: '',
        component: ListOrdersComponent,
        data: {
          title: 'List My Orders',
        },
      },
      {
        path: ':orderId/items',
        component: ListOrderItemsComponent,
        data: {
          title: 'List Order Items',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
