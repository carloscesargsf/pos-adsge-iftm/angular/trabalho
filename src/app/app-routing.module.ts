import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultLayoutComponent } from './core/layouts/default-layout/default-layout.component';
import { Status404Component } from './view/errors/status-404/status-404.component';
import { AuthGuard } from './core/guards/auth.guard';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'categories',
    pathMatch: 'full',
  },
  {
    path: '',
    loadChildren: () => import('./view/auth/auth.module').then(m => m.AuthModule),
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'products',
        loadChildren: () => import('./view/products/products.module').then(m => m.ProductsModule),
        canActivate: [ AuthGuard ],
      },
      {
        path: 'categories',
        loadChildren: () => import('./view/categories/categories.module').then(m => m.CategoriesModule),
        canActivate: [ AuthGuard ],
      },
      {
        path: 'users',
        loadChildren: () => import('./view/users/users.module').then(m => m.UsersModule),
        canActivate: [ AuthGuard ],
      },
      {
        path: 'my-orders',
        loadChildren: () => import('./view/orders/orders.module').then(m => m.OrdersModule),
        canActivate: [ AuthGuard ],
      },
      {
        path: 'error',
        loadChildren: () => import('./view/errors/errors.module').then(m => m.ErrorsModule),
      },
    ]
  },
  {
    path: '**',
    component: Status404Component
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
