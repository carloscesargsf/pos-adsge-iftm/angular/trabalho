import { FormGroup, FormControl } from '@angular/forms';

export class Category {
  id: number;
  name: string;

  static getFormGroup(): FormGroup {
    return new FormGroup({
      id: new FormControl(null, []),
      name: new FormControl('', []),
    });
  }
}
