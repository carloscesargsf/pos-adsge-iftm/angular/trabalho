import { FormGroup, FormControl, Validators } from '@angular/forms';

export class User {
  id: number;
  name: string;
  email: string;
  phone: string;
  password: string;

  static getFormGroup(): FormGroup {
    return new FormGroup({
      id: new FormControl(null, []),
      name: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(80)]),
      email: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(20)]),
      password: new FormControl('', [Validators.required]),
    });
  }
}
