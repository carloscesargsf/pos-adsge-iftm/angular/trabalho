import { FormGroup, FormControl } from '@angular/forms';

export class Product {
  id: number;
  name: string;
  description: string;
  price: number;
  imgUrl: string;

  static getFormGroup(): FormGroup {
    return new FormGroup({
      id: new FormControl(null, []),
      name: new FormControl('', []),
      description: new FormControl('', []),
      price: new FormControl(null, []),
      imgUrl: new FormControl('', []),
    });
  }
}
