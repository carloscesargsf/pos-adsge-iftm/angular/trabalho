export class OrderItem {
  quantity: number;
  price: number;
  productId: number;
  productName: string;
  productImgUrl: string;
}
