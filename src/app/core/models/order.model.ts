import { OrderStatus } from '../enums/order-status.enum';

export class Order {
  id: number;
  moment: Date;
  orderStatus: OrderStatus;
  clientId: number;
  clientName: string;
  clientEmail: string;
}
