export class OrderStatus {
  static readonly WAITING_PAYMENT = new OrderStatus('WAITING_PAYMENT', 'Waiting Payment');
  static readonly PAID = new OrderStatus('PAID', 'Paid');
  static readonly SHIPPED = new OrderStatus('SHIPPED', 'Shipped');
  static readonly DELIVERED = new OrderStatus('DELIVERED', 'Delivered');
  static readonly CANCELLED = new OrderStatus('CANCELLED', 'Cancelled');

  constructor(private readonly key: string, public readonly label: string) { }

  toString() {
    return this.key;
  }
}
