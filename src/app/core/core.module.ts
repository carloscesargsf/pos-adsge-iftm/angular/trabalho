import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import localePtBr from '@angular/common/locales/pt';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ProductsService } from './services/products/products.service';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { ErrorInterceptor } from './interceptors/error-interceptor';
import { JwtInterceptor } from './interceptors/jwt-interceptor';
import { CategoriesService } from './services/categories/categories.service';
import { AuthService } from './services/auth/auth.service';
import { OrdersService } from './services/orders/orders.service';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {
  decimalMarker: ',',
  thousandSeparator: '.',
  suffix: '',
  showTemplate: true,
};

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    NgxMaskModule.forRoot(options),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    AuthService,
    CategoriesService,
    ProductsService,
    OrdersService,
  ]
})
export class CoreModule {

  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule has already been loaded. You should only import it in the AppModule.');
    }
    registerLocaleData(localePtBr);
  }

}
