import { Component, OnInit } from '@angular/core';
import { navItems } from '../../../_nav';
import { INavData } from '@coreui/angular';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { UsersService } from '../../services/users/users.service';
import { Token } from '../../models/token.model';
import { User } from '../../models/user.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-default-layout',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.scss']
})
export class DefaultLayoutComponent implements OnInit {

  public sidebarMinimized: boolean;
  public navItems: INavData[];

  constructor(private usersService: UsersService,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.sidebarMinimized = false;
    this.navItems = navItems;
  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  profile() {
    this.usersService.findByEmail(this.authService.getTokenValue().email).subscribe(
      (user: User) => this.router.navigate(['/users', user.id]),
      () => this.logout(),
    );
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}
