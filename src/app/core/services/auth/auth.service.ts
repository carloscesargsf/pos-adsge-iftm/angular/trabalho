import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Credentials } from '../../models/credentials.model';
import { map } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { Token } from '../../models/token.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends ApiService {

  private tokenSubject: BehaviorSubject<Token>;
  private token: Observable<Token>;

  constructor(http: HttpClient) {
    super(`${environment.apiUrl}auth`, http);
    this.tokenSubject = new BehaviorSubject<Token>(JSON.parse(localStorage.getItem('token')));
    this.token = this.tokenSubject.asObservable();
  }

  public getTokenValue(): Token {
    return this.tokenSubject.value;
  }

  login(credentials: Credentials): Observable<Token> {
    return this.request('post', '/login', credentials).pipe(
      map(token => {
        localStorage.setItem('token', JSON.stringify(token));
        this.tokenSubject.next(token);
        return token;
      }),
    );
  }

  logout(): void {
    localStorage.removeItem('token');
    this.tokenSubject.next(null);
  }

}
