import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { PageInfo } from '../models/page-info';
import { ApiService } from './api.service';

export abstract class ReadService<E, ID> extends ApiService {

  constructor(protected url: string, http: HttpClient) {
    super(url, http);
  }

  findAll(params: string = null): Observable<PageInfo<E>> {
    return this.http.get<PageInfo<E>[]>(this.url, this.getRequestOptions(null, params))
      .pipe(
        map(data => {
          // tslint:disable-next-line: no-string-literal
          return new PageInfo<E>(data['number'], data['size'], data['totalElements'], data['totalPages'], data['content']);
        })
      );
  }

}
