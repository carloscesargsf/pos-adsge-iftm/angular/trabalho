import { Injectable } from '@angular/core';
import { User } from '../../models/user.model';
import { CrudService } from '../crud.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends CrudService<User, number> {

  constructor(http: HttpClient) {
    super(`${environment.apiUrl}users`, http);
  }

  findByEmail(email: string): Observable<User> {
    return this.request('get', '/find', null, `email=${email}`);
  }

}
