import { Injectable } from '@angular/core';
import { Category } from '../../models/category.model';
import { CrudService } from '../crud.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService extends CrudService<Category, number> {

  constructor(http: HttpClient) {
    super(`${environment.apiUrl}categories`, http);
  }

}
