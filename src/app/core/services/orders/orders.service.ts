import { Injectable } from '@angular/core';
import { Order } from '../../models/order.model';
import { ApiService } from '../api.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { OrderItem } from '../../models/order-item.model';

@Injectable({
  providedIn: 'root'
})
export class OrdersService extends ApiService {

  constructor(http: HttpClient) {
    super(`${environment.apiUrl}orders`, http);
  }

  myOrders(): Observable<Order[]> {
    return this.request('get', '/myorders');
  }

  getOrderItems(id: number): Observable<OrderItem[]> {
    return this.request('get', `/${id}/items`);
  }

}
