import { Injectable } from '@angular/core';
import { Product } from '../../models/product.model';
import { CrudService } from '../crud.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService extends CrudService<Product, number> {

  constructor(http: HttpClient) {
    super(`${environment.apiUrl}products`, http);
  }

}
