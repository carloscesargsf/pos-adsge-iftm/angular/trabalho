import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ReadService } from './read.service';

export abstract class CrudService<E, ID> extends ReadService<E, ID> {

  constructor(protected url: string, http: HttpClient) {
    super(url, http);
  }

  findById(id: ID): Observable<E> {
    return this.http.get<E>(`${this.url}/${id}`);
  }

  create(entity: E): Observable<E> {
    return this.http.post<E>(this.url, entity);
  }

  update(id: any, entity: E): Observable<E> {
    return this.http.put<E>(`${this.url}/${id}`, entity, {});
  }

  delete(id: ID): Observable<void> {
    return this.http.delete<void>(`${this.url}/${id}`);
  }

}
