import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CurrencyBrlPipe } from './pipes/currency-brl.pipe';
import { NgxMaskModule } from 'ngx-mask';
import { NgxCurrencyModule } from 'ngx-currency';

@NgModule({
  declarations: [
    CurrencyBrlPipe,
  ],
  providers: [
    CurrencyPipe
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CurrencyBrlPipe,
    NgxMaskModule,
    NgxCurrencyModule,
  ],
})
export class SharedModule { }
