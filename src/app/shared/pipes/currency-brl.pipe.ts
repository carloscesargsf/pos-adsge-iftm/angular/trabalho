import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Pipe({
  name: 'currencyBrl'
})
export class CurrencyBrlPipe implements PipeTransform {

  constructor(private currencyPipe: CurrencyPipe) { }

  transform(value: any, ...args: any[]): any {
    const currencyCode = 'BRL';
    const display = 'symbol';
    const digits = '1.2-2';
    const locale = 'pt';
    return this.currencyPipe.transform(value, currencyCode, display, digits, locale);
  }

}
